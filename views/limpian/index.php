<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Limpians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="limpian-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Limpian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_limpieza',
            'dni',
            'dir_completa',
            'duracion_servicio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
