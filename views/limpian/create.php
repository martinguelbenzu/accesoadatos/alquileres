<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Limpian */

$this->title = 'Create Limpian';
$this->params['breadcrumbs'][] = ['label' => 'Limpians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="limpian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
