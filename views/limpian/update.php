<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Limpian */

$this->title = 'Update Limpian: ' . $model->id_limpieza;
$this->params['breadcrumbs'][] = ['label' => 'Limpians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_limpieza, 'url' => ['view', 'id' => $model->id_limpieza]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="limpian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
