<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alojamientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alojamientos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alojamientos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_limpieza',
            'dni_cliente',
            'dni_inquilino',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
