<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alojamientos */

$this->title = 'Create Alojamientos';
$this->params['breadcrumbs'][] = ['label' => 'Alojamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alojamientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
