<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alojamientos */

$this->title = 'Update Alojamientos: ' . $model->id_limpieza;
$this->params['breadcrumbs'][] = ['label' => 'Alojamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_limpieza, 'url' => ['view', 'id' => $model->id_limpieza]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alojamientos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
