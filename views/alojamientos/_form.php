<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alojamientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alojamientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dni_inquilino')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
