<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Utensilios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utensilios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_limpieza')->textInput() ?>

    <?= $form->field($model, 'utensilio')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
