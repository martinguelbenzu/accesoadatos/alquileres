<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Utensilios */

$this->title = 'Create Utensilios';
$this->params['breadcrumbs'][] = ['label' => 'Utensilios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utensilios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
