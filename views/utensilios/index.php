<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Utensilios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utensilios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Utensilios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_utensilios',
            'id_limpieza',
            'utensilio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
