<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Utensilios */

$this->title = 'Update Utensilios: ' . $model->id_utensilios;
$this->params['breadcrumbs'][] = ['label' => 'Utensilios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_utensilios, 'url' => ['view', 'id' => $model->id_utensilios]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="utensilios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
