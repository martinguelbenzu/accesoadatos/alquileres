<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Propietarios Propiedades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="propietarios-propiedades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Propietarios Propiedades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_posesion',
            'dni',
            'dir_completa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
