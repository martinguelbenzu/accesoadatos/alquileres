<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropietariosPropiedades */

$this->title = 'Update Propietarios Propiedades: ' . $model->id_posesion;
$this->params['breadcrumbs'][] = ['label' => 'Propietarios Propiedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_posesion, 'url' => ['view', 'id' => $model->id_posesion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="propietarios-propiedades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
