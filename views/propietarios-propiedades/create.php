<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropietariosPropiedades */

$this->title = 'Create Propietarios Propiedades';
$this->params['breadcrumbs'][] = ['label' => 'Propietarios Propiedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="propietarios-propiedades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
