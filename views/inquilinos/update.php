<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inquilinos */

$this->title = 'Update Inquilinos: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Inquilinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'id' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inquilinos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
