<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inquilinos */

$this->title = 'Create Inquilinos';
$this->params['breadcrumbs'][] = ['label' => 'Inquilinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inquilinos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
