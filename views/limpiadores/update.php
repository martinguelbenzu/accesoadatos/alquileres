<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Limpiadores */

$this->title = 'Update Limpiadores: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Limpiadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'id' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="limpiadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
