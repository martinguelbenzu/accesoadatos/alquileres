<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Limpiadores */

$this->title = 'Create Limpiadores';
$this->params['breadcrumbs'][] = ['label' => 'Limpiadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="limpiadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
