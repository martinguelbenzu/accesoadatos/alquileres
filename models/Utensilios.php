<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utensilios".
 *
 * @property int $id_utensilios
 * @property int $id_limpieza
 * @property string $utensilio
 *
 * @property Limpian $limpieza
 */
class Utensilios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utensilios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_limpieza', 'utensilio'], 'required'],
            [['id_limpieza'], 'integer'],
            [['utensilio'], 'string', 'max' => 30],
            [['id_limpieza'], 'unique'],
            [['id_limpieza'], 'exist', 'skipOnError' => true, 'targetClass' => Limpian::className(), 'targetAttribute' => ['id_limpieza' => 'id_limpieza']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_utensilios' => 'Id Utensilios',
            'id_limpieza' => 'Id Limpieza',
            'utensilio' => 'Utensilio',
        ];
    }

    /**
     * Gets query for [[Limpieza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLimpieza()
    {
        return $this->hasOne(Limpian::className(), ['id_limpieza' => 'id_limpieza']);
    }
}
