<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propietarios_propiedades".
 *
 * @property int $id_posesion
 * @property string $dni
 * @property string $dir_completa
 *
 * @property Propiedades $dirCompleta
 * @property Propietarios $dni0
 */
class PropietariosPropiedades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propietarios_propiedades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'dir_completa'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['dir_completa'], 'string', 'max' => 50],
            [['dni', 'dir_completa'], 'unique', 'targetAttribute' => ['dni', 'dir_completa']],
            [['dir_completa'], 'exist', 'skipOnError' => true, 'targetClass' => Propiedades::className(), 'targetAttribute' => ['dir_completa' => 'dir_completa']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Propietarios::className(), 'targetAttribute' => ['dni' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_posesion' => 'Id Posesion',
            'dni' => 'Dni',
            'dir_completa' => 'Dir Completa',
        ];
    }

    /**
     * Gets query for [[DirCompleta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirCompleta()
    {
        return $this->hasOne(Propiedades::className(), ['dir_completa' => 'dir_completa']);
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Propietarios::className(), ['dni' => 'dni']);
    }
}
