<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propietarios".
 *
 * @property string $dni
 * @property string $tlf
 * @property string $e_mail
 *
 * @property PropietariosPropiedades[] $propietariosPropiedades
 * @property Propiedades[] $dirCompletas
 */
class Propietarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propietarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf', 'e_mail'], 'required'],
            [['dni', 'tlf'], 'string', 'max' => 9],
            [['e_mail'], 'string', 'max' => 50],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'e_mail' => 'E Mail',
        ];
    }

    /**
     * Gets query for [[PropietariosPropiedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropietariosPropiedades()
    {
        return $this->hasMany(PropietariosPropiedades::className(), ['dni' => 'dni']);
    }

    /**
     * Gets query for [[DirCompletas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirCompletas()
    {
        return $this->hasMany(Propiedades::className(), ['dir_completa' => 'dir_completa'])->viaTable('propietarios_propiedades', ['dni' => 'dni']);
    }
}
