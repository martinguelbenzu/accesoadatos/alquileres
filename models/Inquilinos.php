<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inquilinos".
 *
 * @property string $dni
 * @property string $tlf
 * @property string $f_nac
 *
 * @property Alojamientos $alojamientos
 */
class Inquilinos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inquilinos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf', 'f_nac'], 'required'],
            [['f_nac'], 'safe'],
            [['dni', 'tlf'], 'string', 'max' => 9],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'f_nac' => 'F Nac',
        ];
    }

    /**
     * Gets query for [[Alojamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlojamientos()
    {
        return $this->hasOne(Alojamientos::className(), ['dni_inquilino' => 'dni']);
    }
}
