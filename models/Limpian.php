<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "limpian".
 *
 * @property int $id_limpieza
 * @property string $dni
 * @property string $dir_completa
 * @property float $duracion_servicio
 *
 * @property Limpiadores $dni0
 * @property Propiedades $dirCompleta
 * @property Utensilios $utensilios
 */
class Limpian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'limpian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'dir_completa', 'duracion_servicio'], 'required'],
            [['duracion_servicio'], 'number'],
            [['dni'], 'string', 'max' => 9],
            [['dir_completa'], 'string', 'max' => 50],
            [['dni', 'dir_completa'], 'unique', 'targetAttribute' => ['dni', 'dir_completa']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Limpiadores::className(), 'targetAttribute' => ['dni' => 'dni']],
            [['dir_completa'], 'exist', 'skipOnError' => true, 'targetClass' => Propiedades::className(), 'targetAttribute' => ['dir_completa' => 'dir_completa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_limpieza' => 'Id Limpieza',
            'dni' => 'Dni',
            'dir_completa' => 'Dir Completa',
            'duracion_servicio' => 'Duracion Servicio',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Limpiadores::className(), ['dni' => 'dni']);
    }

    /**
     * Gets query for [[DirCompleta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirCompleta()
    {
        return $this->hasOne(Propiedades::className(), ['dir_completa' => 'dir_completa']);
    }

    /**
     * Gets query for [[Utensilios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtensilios()
    {
        return $this->hasOne(Utensilios::className(), ['id_limpieza' => 'id_limpieza']);
    }
}
