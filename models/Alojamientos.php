<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alojamientos".
 *
 * @property int $id_limpieza
 * @property string $dni_cliente
 * @property string $dni_inquilino
 *
 * @property Clientes $dniCliente
 * @property Inquilinos $dniInquilino
 */
class Alojamientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alojamientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_cliente', 'dni_inquilino'], 'required'],
            [['dni_cliente', 'dni_inquilino'], 'string', 'max' => 9],
            [['dni_cliente'], 'unique'],
            [['dni_inquilino'], 'unique'],
            [['dni_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['dni_cliente' => 'dni']],
            [['dni_inquilino'], 'exist', 'skipOnError' => true, 'targetClass' => Inquilinos::className(), 'targetAttribute' => ['dni_inquilino' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_limpieza' => 'Id Limpieza',
            'dni_cliente' => 'Dni Cliente',
            'dni_inquilino' => 'Dni Inquilino',
        ];
    }

    /**
     * Gets query for [[DniCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCliente()
    {
        return $this->hasOne(Clientes::className(), ['dni' => 'dni_cliente']);
    }

    /**
     * Gets query for [[DniInquilino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniInquilino()
    {
        return $this->hasOne(Inquilinos::className(), ['dni' => 'dni_inquilino']);
    }
}
