<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "limpiadores".
 *
 * @property string $dni
 * @property string $tlf
 * @property float|null $horas_mensuales
 *
 * @property Limpian[] $limpians
 * @property Propiedades[] $dirCompletas
 */
class Limpiadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'limpiadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf'], 'required'],
            [['horas_mensuales'], 'number'],
            [['dni', 'tlf'], 'string', 'max' => 9],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'horas_mensuales' => 'Horas Mensuales',
        ];
    }

    /**
     * Gets query for [[Limpians]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLimpians()
    {
        return $this->hasMany(Limpian::className(), ['dni' => 'dni']);
    }

    /**
     * Gets query for [[DirCompletas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirCompletas()
    {
        return $this->hasMany(Propiedades::className(), ['dir_completa' => 'dir_completa'])->viaTable('limpian', ['dni' => 'dni']);
    }
}
