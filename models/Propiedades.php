<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propiedades".
 *
 * @property string $dir_completa
 * @property int $habitaciones
 * @property int|null $ascensor
 *
 * @property Clientes $clientes
 * @property Limpian[] $limpians
 * @property Limpiadores[] $dnis
 * @property PropietariosPropiedades[] $propietariosPropiedades
 * @property Propietarios[] $dnis0
 */
class Propiedades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propiedades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dir_completa', 'habitaciones'], 'required'],
            [['habitaciones', 'ascensor'], 'integer'],
            [['dir_completa'], 'string', 'max' => 50],
            [['dir_completa'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dir_completa' => 'Dir Completa',
            'habitaciones' => 'Habitaciones',
            'ascensor' => 'Ascensor',
        ];
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasOne(Clientes::className(), ['dir_completa' => 'dir_completa']);
    }

    /**
     * Gets query for [[Limpians]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLimpians()
    {
        return $this->hasMany(Limpian::className(), ['dir_completa' => 'dir_completa']);
    }

    /**
     * Gets query for [[Dnis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnis()
    {
        return $this->hasMany(Limpiadores::className(), ['dni' => 'dni'])->viaTable('limpian', ['dir_completa' => 'dir_completa']);
    }

    /**
     * Gets query for [[PropietariosPropiedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropietariosPropiedades()
    {
        return $this->hasMany(PropietariosPropiedades::className(), ['dir_completa' => 'dir_completa']);
    }

    /**
     * Gets query for [[Dnis0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnis0()
    {
        return $this->hasMany(Propietarios::className(), ['dni' => 'dni'])->viaTable('propietarios_propiedades', ['dir_completa' => 'dir_completa']);
    }
}
