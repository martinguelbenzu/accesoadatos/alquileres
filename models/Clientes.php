<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $dni
 * @property string $tlf
 * @property string $f_nac
 * @property float $precio_firmado
 * @property string $dir_completa
 *
 * @property Alojamientos $alojamientos
 * @property Propiedades $dirCompleta
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf', 'f_nac', 'precio_firmado', 'dir_completa'], 'required'],
            [['f_nac'], 'safe'],
            [['precio_firmado'], 'number'],
            [['dni', 'tlf'], 'string', 'max' => 9],
            [['dir_completa'], 'string', 'max' => 50],
            [['dir_completa'], 'unique'],
            [['dni'], 'unique'],
            [['dir_completa'], 'exist', 'skipOnError' => true, 'targetClass' => Propiedades::className(), 'targetAttribute' => ['dir_completa' => 'dir_completa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'f_nac' => 'F Nac',
            'precio_firmado' => 'Precio Firmado',
            'dir_completa' => 'Dir Completa',
        ];
    }

    /**
     * Gets query for [[Alojamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlojamientos()
    {
        return $this->hasOne(Alojamientos::className(), ['dni_cliente' => 'dni']);
    }

    /**
     * Gets query for [[DirCompleta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirCompleta()
    {
        return $this->hasOne(Propiedades::className(), ['dir_completa' => 'dir_completa']);
    }
}
